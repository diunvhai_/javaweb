package com.ixf.servlet;

import sun.rmi.log.LogOutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class IncludingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out =resp.getWriter();
        RequestDispatcher dispatcher =req.getRequestDispatcher("/IncludedServlet?p1=abc");
        out.println("before including 次艘"+"<br>");
        out.println("中文");
        dispatcher.include(req,resp);
        out.println("after including"+"<br>");
        out.println("中文");


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}

package com.ixf.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        //没有输出97是因为输出的是ascii 97-122，a~z
        out.write(97);
        out.println(97);

        //得到ServletContext对象
//        ServletContext context = this.getServletContext();
//        //得到所有初始化为Enumeration对象
//        Enumeration<String> paramNames = context.getInitParameterNames();
//         out.println("all the paramName and paramName and paramValue are floowing:");
//         //遍历所有初始化参数名
//         while (paramNames.hasMoreElements()){
//             String name = paramNames.nextElement();
//             //通过getInitParameter方法得到对应的参数值
//             String value = context.getInitParameter(name);
//             out.print(name + ":" + value);
//             out.println("<br />");
//
//         }
        //如何获取servletconfig对象中的name数据值
        //String address =  this.getServletContext().getInitParameter("address");
        //String name = this.getServletConfig().getInitParameter("name");



    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }
}
